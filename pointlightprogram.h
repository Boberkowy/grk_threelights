#ifndef POINTLIGHTPROGRAM_H
#define POINTLIGHTPROGRAM_H

#include <GL/glew.h>

#include "program.h"
#include "light.h"


class PointLightProgram : public Program{
public:
    void SetLight(const PointLight & light) const;
    void Initialize(const char *vertex_shader_file, const char *fragment_shader_file);
private:
    struct {
        GLuint ambient;
        GLuint attenuation;
        GLuint diffuse;
        GLuint position;
        GLuint specular;
    }light_locations_;
};

#endif // POINTLIGHTPROGRAM_H
