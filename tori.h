#ifndef TORI_H
#define TORI_H

#include <GL/glew.h>
#include<gl/GL.h>
#include "program.h"
#include "model.h"
#include "matma.h"

class Tori: public Model{
 public:
    Tori(GLfloat init_velocity=10, GLfloat init_angle=0);
    void SetMaterialEarth(const Material & material){material_sun_ = material;}
	void SetMaterialMoon(const Material & material){ material_moon_ = material; }
    void SetInitAngle(GLfloat angle){angle_= angle;}
    void SetVelocity(GLfloat velocity){velocity_= velocity;}
    void Initialize(int n, int m, GLfloat R, GLfloat r);
    void Draw(const Program & program) const;
    void Move(GLfloat delta_t);
    void SpeedUp();
    void SlowDown();
    void ToggleAnimated();
	void SetTextureMoon(GLuint t){ texture_moon_ = t; }
	void SetTextureSun(GLuint t){ texture_sun_ = t; }
 private:
    int n_; // mesh parameter
    int m_; // mesh parameter

    Mat4 model_matrix_sun_;
	Mat4 model_matrix_moon_;

    Mat3 normal_matrix_sun_;
	Mat3 normal_matrix_moon_;


    Material material_moon_;
	Material material_sun_;

	GLuint texture_moon_;
	GLuint texture_sun_;

    GLfloat R; //big radius
    GLfloat r; //small radius

    GLfloat angle_;
    GLfloat velocity_;
    bool animated_;

};

#endif // TORI_H
