#ifndef MODEL_H
#define MODEL_H

#include <GL/glew.h>
#include <iostream>
#include "matma.h"
#include "material.h"

typedef struct{
    float position[4];
    float color[4];
} ColorVertex;

typedef struct{
    float position[4];
    float texture[2];
} TextureVertex;

typedef struct{
    float position[4];
    float texture[2];
    float normal[3];
} NormalTextureVertex;


class Model{
public:
    ~Model();
    void SetTextureUnit(GLuint t){texture_unit_=t;}
    void SetTexture(GLuint t){texture_ = t;}
    void SetMaterial(const Material & material){material_=material;}
protected:
    Material material_;
    Mat4 model_matrix_;
    Mat3 normal_matrix_;
    GLuint vao_;
    GLuint vertex_buffer_;
    GLuint index_buffer_;
    GLuint texture_unit_;
    GLuint texture_;
};

#endif // MODEL_H
